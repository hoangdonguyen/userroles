Feature: NBA Drupal Admin Page
  In order to work on the NBA Admin page
  As a editor
  I need to be able to use NBA Admin page powered by Drupal 8


#  Scenario Outline: verify that an admin can create different user roles
#
#    #######  an HTTP proxy authentication popup ==> embed the usrname:passwd to URL
#    ######
#    Given I go to "https://access:NBA+Marty!Dev16@publish.qa.nba.com/user/login"
#    And I see the "Log in" link
#    Then I click the "Log in" link
#
#    Given I see "Enter your NBA.com username." whose id is "edit-name--description" on the current page
#    When I enter "marty" in the "Username" input box
#    And I enter "NBAmarty-2016" in the "Password" input box
#    Then I press the "Log in" button
#
#    Given I search for the "People" link by clicking the "Manage" toolbar icon
#    When I see the "People" link
#    # the People link can be seen but cannot be clicked if the Mink function
#    # findLink() is used.  Only Content, Structure, Configuration and Reports
#    # links can be seen and be clicked
#    Then I click the "People" link whose href is "/admin/people"
#
#    Given I click the "Add user" link
#    And I see "Add user" whose id is "block-seven-page-title" on the current page
#    When I click on the checkbox of "<Role>" whose id is "<Role ID>"
#    And I enter "<usrname>" in the "Username" input box
#    And I enter "<passwd>" in the "Password" input box
#    And I enter "<passwd>" in the "Confirm password" input box
#    Then I press the "Create new account" button
#
#    Given I click the "marty" toolbar icon
#    When I click the "Log out" link
#    Then I close the web browser

#    Examples:
#      |Role              |Role ID                      |usrname     |passwd       |
#      |Super Publisher   |edit-roles-super-publisher   |sPub-auto   |NBAmarty-2016|
#      |Internal Publisher|edit-roles-internal-publisher|iPub-auto   |NBAmarty-2016|
#      |External Publisher|edit-roles-external-publisher|ePub-auto   |NBAmarty-2016|
#      |Contributor       |edit-roles-contributor       |contrbt-auto|NBAmarty-2016|


  Scenario: verify that a Contributor can create new Article content

    #######  an HTTP proxy authentication popup ==> embed the usrname:passwd to URL
    #######
    Given I go to "https://access:NBA+Marty!Dev16@publish.qa.nba.com/user/login"
    And I see the "Log in" link
    Then I click the "Log in" link

    Given I see "Enter your NBA.com username." whose id is "edit-name--description" on the current page
    When I enter "contrbt-auto" in the "Username" input box
    And I enter "NBAmarty-2016" in the "Password" input box
    Then I press the "Log in" button
    And I wait for "7" seconds

    Given I click the "Shortcuts" toolbar icon
    When I see the "Add content" link
    # the People link can be seen but cannot be clicked if the Mink function
    # findLink() is used.  Only Content, Structure, Configuration and Reports
    # links can be seen and be clicked
    Then I click the "Add content" link

#  Scenario: verify that a Contributor can edit their own Article
#
#    #######  an HTTP proxy authentication popup ==> embed the usrname:passwd to URL
#    #######
#    Given I go to "https://access:NBA+Marty!Dev16@publish.qa.nba.com/user/login"
#    And I see the "Log in" link
#    Then I click the "Log in" link
#
#    Given I see "Enter your NBA.com username." whose id is "edit-name--description" on the current page
#    When I enter "contrbt-auto" in the "Username" input box
#    And I enter "NBAmarty-2016" in the "Password" input box
#    Then I press the "Log in" button

#  Scenario: verify that a Contributor can view their own revisions
#
#    #######  an HTTP proxy authentication popup ==> embed the usrname:passwd to URL
#    #######
#    Given I go to "https://access:NBA+Marty!Dev16@publish.qa.nba.com/user/login"
#    And I see the "Log in" link
#    Then I click the "Log in" link
#
#    Given I see "Enter your NBA.com username." whose id is "edit-name--description" on the current page
#    When I enter "contrbt-auto" in the "Username" input box
#    And I enter "NBAmarty-2016" in the "Password" input box
#    Then I press the "Log in" button

#  Scenario: verify that a Contributor can revert their own article revisions
#
#    #######  an HTTP proxy authentication popup ==> embed the usrname:passwd to URL
#    #######
#    Given I go to "https://access:NBA+Marty!Dev16@publish.qa.nba.com/user/login"
#    And I see the "Log in" link
#    Then I click the "Log in" link
#
#    Given I see "Enter your NBA.com username." whose id is "edit-name--description" on the current page
#    When I enter "contrbt-auto" in the "Username" input box
#    And I enter "NBAmarty-2016" in the "Password" input box
#    Then I press the "Log in" button

#  Scenario Outline: verify that an admin can delete different user roles
#
#    #######  an HTTP proxy authentication popup ==> embed the usrname:passwd to URL
#    #######
#    Given I go to "https://access:NBA+Marty!Dev16@publish.qa.nba.com/user/login"
#    And I see the "Log in" link
#    Then I click the "Log in" link
#
#    Given I see "Enter your NBA.com username." whose id is "edit-name--description" on the current page
#    When I enter "marty" in the "Username" input box
#    And I enter "NBAmarty-2016" in the "Password" input box
#    Then I press the "Log in" button
#
#    Given I search for the "People" link by clicking the "Manage" toolbar icon
#    When I see the "People" link
#    Then I click the "People" link whose href is "/admin/people"
#
#    Given I see the "<usrname>" link
#    And I click the "<usrname>" link
#    And I wait for "3" seconds
#    When I press the "Edit" button - "anchor text"
#    And I wait for "3" seconds
#    And I press the "Cancel account" button
#    And I wait for "3" seconds
#    Then I select the radio button - "id" of "edit-user-cancel-method-user-cancel-delete"
#    And I press the "Cancel account" button
#    And I wait for "1" seconds
#    And I see "<Message>" of element "div.messages--status" in the toolbar region
#
#    Given I click the "marty" toolbar icon
#    When I click the "Log out" link
#    Then I close the web browser
#
#    Examples:
#      |usrname     |Message                       |
#      |contrbt-auto|contrbt-auto has been deleted.|
#      |iPub-auto   |iPub-auto has been deleted.   |
#      |ePub-auto   |ePub-auto has been deleted.   |
#      |sPub-auto   |sPub-auto has been deleted.   |