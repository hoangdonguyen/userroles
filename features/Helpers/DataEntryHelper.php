<?php
/**
 * Created by PhpStorm.
 * User: hnguyen
 * Date: 4/6/2016
 * Time: 3:55 PM
 */

function dataEntry($page, $fieldName, $fieldData)
{
    $page->findField($fieldName)->setValue($fieldData);
}